function runningMedian(array) {
    if(array.length === 1) return array[0]
    if(array.length === 0) return 0

    const medians = []
    for(let i=1; i <= array.length; i++) {
        const test = array.slice(0, i).sort((a, b) => a - b)
        const midPoint = Math.floor(test.length / 2);

        const median = test.length % 2 !== 0 ? test[midPoint] : (test[midPoint-1] + test[midPoint]) / 2
        medians.push(median)
    }
    return medians
}

// console.log(runningMedian([7,3,5,2]))

console.log(runningMedian([12, 4, 5, 3, 8, 7]))  // [ 12, 8, 8.5, 3.5, 10, 4.5 ]
/*
12.0
8.0
5.0
4.5
5.0
6.0
*/