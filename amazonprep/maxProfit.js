function maxProfit(inventory, order){
    let res = 0
    while (order > 0) {
        const max = Math.max(...inventory)
        inventory[inventory.indexOf(max)]--
        res += max
        order--
    }
    return res
}

maxProfit([6,4], 4) // 19