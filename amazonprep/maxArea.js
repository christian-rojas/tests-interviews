function maxArea(height) {

    // definir 2 pointers, inicial y final 
    let ans = 0, i = 0, j = height.length-1
    while (i < j) {
        // buscar el mayor entre el anterior y el minimo entre la barra izquiera y derecha
        // multiplicada por la distancia horizontal
        console.log(ans, height[i], height[j], i, j);
        ans = Math.max(ans, Math.min(height[i], height[j]) * (j - i))
        // altura de la izquierda es menor o igual a la altura derecha
        // incrementa la izquierda o derecha
        height[i] <= height[j] ? i++ : j--
    }
    return ans
    
    
};

maxArea([1,8,6,2,5,4,8,3,7])