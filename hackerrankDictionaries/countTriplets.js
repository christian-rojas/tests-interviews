// Complete the countTriplets function below.
function countTriplets(arr, r) {
	const hGram = {};
	const hGram2 = {};
    // suma final
	let count = 0;
    // si es menor a 3 no se puede
	if (arr.length < 3) return 0;
    // recorrer el arreglo hacia atras
	for (let i = arr.length - 1; i >= 0; i--) {
        // primer elemento
		let t1 = arr[i];
		let t2 = t1 * r;
		let t3 = t2 * r;
        // sumar al contador si existe, en la primera no existe
		count += hGram2[t3] || 0;
        // 
        console.log(hGram2[t3], t3, count);
		hGram2[t2]
			? (hGram2[t2] += hGram[t2] || 0)
			: (hGram2[t2] = hGram[t2] || 0);
        
		hGram[t1] ? hGram[t1]++ : (hGram[t1] = 1);
	}
	return count;
}

const arr = [1, 3, 9, 9, 27, 81];
const r = 3;

console.log(countTriplets(arr, r));
