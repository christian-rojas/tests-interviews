function sherlockAndAnagrams(s) {
    // crear Map
    const map = new Map();
    // recorrer el string
    for (let i = 0; i < s.length; ++i) {
        // recorrer desde la posicion actual
        for (let j = i; j < s.length; ++j) {
            // obtengo el caracter actual
            const sub = s.substring(i, j + 1);
            const key = sub.split('').sort().join('');
            // si el map ya tiene el caracter
            if (map.has(key)) {
                // aumento en 1
                map.set(key, map.get(key) + 1); 
            } else {
                // asigno 1
                map.set(key, 1);
            }
        }
    }
    let result = 0;
    for (const [key, value] of map) {
        // si la key tiene valor mayor a 1, osea que se repite
        if (value > 1) {
            // incremento el valor final
            result += countPairs(value);    
        }
    }
    return result;
}
// cuento los pares, divisible por 2
function countPairs(n) {
    return (n * (n - 1)) / 2;
}

console.log(sherlockAndAnagrams('kkkk'))