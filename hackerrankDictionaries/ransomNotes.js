function checkMagazine(magazine, note) {
    for (let word of note) {
        const idx = magazine.indexOf(word)
        if (idx !== -1) {
            magazine[idx] = ""
        } else {
            return console.log('No')
        }
    }
    console.log('Yes')

}

// si las letras contenidas en notes con lowercase estan en magazine devuelve Yes