function twoStrings(s1, s2) {
    let flag = false
    for(const item of s1.split('')){
        if(s2.includes(item)) {
            flag = true
        }
    }
    
    return flag ? "YES" : "NO"
}

const s1 = "hello"
const s2 = "world"
console.log(twoStrings(s1, s2))