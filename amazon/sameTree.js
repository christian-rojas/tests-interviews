function isSameTree(p, q) {
    if(p == null && q == null) return true;
    if(p == null || q == null) return false;
    if(p.val == q.val)
        return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
    return false;
}

console.log(isSameTree([1,2,3], [1,2,3])); // true

/*
    Time complexity: O(n)
    Space complexity: O(1) (ignore recursion stack, otherwise the height of the tree)
*/