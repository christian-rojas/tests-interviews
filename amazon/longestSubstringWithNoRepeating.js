var lengthOfLongestSubstring = function(s) {
    // hashmap ds
    const hashMap = new Map()
    // temporal maximum
    let tempMax = 0
    // result
    let maxLen = 0
    for(let i=0; i<s.length; i++){
        // si el utlimo caracter es igual al actual
        if(hashMap.get(s[i]) >= tempMax){
            // temporal + 1
            tempMax = hashMap.get(s[i]) + 1
        }
        // set char y posicion
        hashMap.set(s[i], i)
        // es mayor el resultado acumulado o (indice - temporal + 1)
        maxLen = Math.max(maxLen, i - tempMax + 1)
    }
    return maxLen
};

console.log(lengthOfLongestSubstring("abcabcbb")); // The answer is "abc", with the length of 3.