var findMedianSortedArrays = function(nums1, nums2) {
    const newArray = [...nums1, ...nums2]
    newArray.sort((a,b) => a - b)
    const middle = Math.ceil(newArray.length / 2)
    const median = newArray.length % 2 === 0 ? (newArray[middle] + newArray[middle-1]) / 2 : Math.ceil(newArray[middle - 1])
    return median
};

console.log(findMedianSortedArrays([1,2], [3,4])); // merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.