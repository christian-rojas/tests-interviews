function makeAnagram(a, b) {
    // delete every char who is not present in the other string
    let freqs = {};
    a.split('').forEach(char => freqs[char] = (freqs[char] || 0) + 1); // increment
    b.split('').forEach(char => freqs[char] = (freqs[char] || 0) - 1); // decrement
    return Object.keys(freqs).reduce((sum, key) => sum + Math.abs(freqs[key]), 0);

    
}
console.log(makeAnagram('cde', 'abc'));
