function taskAssignment(k, tasks) {
    const formattedTasks = tasks.map((value, index) => ({value, index}))
    formattedTasks.sort((a,b) => a.value - b.value)
  
    const res = []
    let start = 0, end = formattedTasks.length - 1
    while(start <= end){
      res.push([formattedTasks[start].index, formattedTasks[end].index])
      start++
      end--
    }
    return res
  }