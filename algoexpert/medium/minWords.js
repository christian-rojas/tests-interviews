function minimumCharactersForWords(words) {
	const maximumCharacterFrequencies = {};
    // itero las palabras
	for (const word of words) {
        // mapa con la frecuencia de cada letra
		const characterFrecuencies = countCharacterFrecuencies(word);
        // actualizo la cantidad maxima de cada letra
		updateMaximumFrecuencies(
			characterFrecuencies,
			maximumCharacterFrequencies
		);
	}
    // retorno el array con las letras final
	return makeArrayFromCharacterFrecuencies(maximumCharacterFrequencies);
}

function countCharacterFrecuencies(string) {
	const characterFrecuencies = {};
	for (const character of string) {
        // si el caracter no esta las frecuencias 
		if (!(character in characterFrecuencies)) {
            // lo dejo en cero
			characterFrecuencies[character] = 0;
		}
        // sino lo incremento
		characterFrecuencies[character] += 1;
	}
    // devuelvo solo los que se repiten
	return characterFrecuencies;
}

function updateMaximumFrecuencies(frecuencies, maximumFrecuencies) {
    //itero las repeticiones
	for (const character in frecuencies) {
        // busco la cantidad de repeticion (frecuencia)
		const frecuency = frecuencies[character];
        // si el caracter esta en la cantidad maxima de repeticion
		if (character in maximumFrecuencies) {
            // actualizo la frecuencia con el maximo valor entre la frecuencia y el valor anterior
			maximumFrecuencies[character] = Math.max(
				frecuency,
				maximumFrecuencies[character]
			);
        // el caracter no esta
		} else {
            // actualizo su maxima frecuencia
			maximumFrecuencies[character] = frecuency;
		}
	}
}
// construir el array
function makeArrayFromCharacterFrecuencies(characterFrecuencies) {
	const characters = [];
	for (const character in characterFrecuencies) {
		const frecuency = characterFrecuencies[character];
		for (let idx = 0; idx < frecuency; idx++) {
			characters.push(character);
		}
	}
	return characters;
}

const words = ['this', 'that', 'did', 'deed', 'them!', 'a'];
const words1 = ['abc', 'ab', 'b', 'bac', 'c'];

console.log(minimumCharactersForWords(words));