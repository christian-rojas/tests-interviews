function minNumberOfCoinsForChange(n, denoms) {
	// crear un array vacio con el mismo numero del n
	const numOfCoins = new Array(n+1).fill(Infinity)
	// empezar por cero
	numOfCoins[0] = 0
	// iterar por las denominaciones
	for(const denom of denoms){
		// iterar por el array de numero de monedas
		for (let amount = 0; amount < numOfCoins.length; amount++) {
			// si la denominacion es menor o igual al monto me sirve
			if(denom <= amount) {
				// reemplazo el valor de la posicion por el minimo valor
				// entre el valor actual (Infinity) o el anterior
				numOfCoins[amount] = Math.min(numOfCoins[amount], numOfCoins[amount - denom] + 1)
			}
		}
	}
	// retornar la ultima posicion con el valor actualizado o -1
	return numOfCoins[n] !== Infinity ? numOfCoins[n] : -1
}

minNumberOfCoinsForChange(9, [3, 4, 5])