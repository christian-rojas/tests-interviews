function staircaseTraversal(height, maxSteps) {
    let currentNumberOfWays = 0
    // siempre es al menos 1
    const waysToTop = [1]
    // iterar sobre la altura mas uno (porque me salto el primero)
    for(let currentHeight = 1; currentHeight < height + 1; currentHeight++){
        // start at 0
        const startOfWindow = currentHeight - maxSteps - 1
        // start at 0
        const endOfWindow = currentHeight - 1
        // si es mayor o igual a cero 
        // le resto el acumulado
        if(startOfWindow >= 0) currentNumberOfWays -= waysToTop[startOfWindow]
        // actualizo el numero de pasos
        currentNumberOfWays += waysToTop[endOfWindow]
        // lo agrego al array
        waysToTop.push(currentNumberOfWays)
    }
  
    return waysToTop[height]
  }

  staircaseTraversal(4, 2) // 5 ways