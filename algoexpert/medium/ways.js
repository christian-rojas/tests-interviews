function numberOfWaysToMakeChange(n, denoms) {
    // creaar un array con el objetivo +1 y rellenar con ceros
      const ways = new Array(n+1).fill(0)
      // el primer elemento es 1
      ways[0] = 1
      //recorrer los elementos
      for(let denom of denoms) {
          // empezar desde el segundo item
          for(let amount = 1; amount < n + 1; amount++) {
              // la valor de la moneda es menor o igual a la iteracion
              if(denom <= amount) {
                // console.log(denom, amount)
                // valor de la posicion actual += 
                console.log(ways[amount - denom]); // siempre es 1
                ways[amount] += ways[amount - denom]
              }
          }
      }
      return ways[n]
  }

numberOfWaysToMakeChange(6, [1,5])