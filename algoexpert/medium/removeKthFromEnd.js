function removeKthNodeFromEnd(head, k) {
    let counter = 1
    let first = head
    let second = head
    while(counter <= k) {
      second = second.next
      counter++
    }
    if(second === null){
      head.value = head.next.value
      head.next = head.next.next
      return
    }
    while(second.next !== null){
      second = second.next
      first = first.next
    }
    first.next = first.next.next
  }

const head = {
    "head": "0",
    "nodes": [
      {"id": "0", "next": "1", "value": 0},
      {"id": "1", "next": "2", "value": 1},
      {"id": "2", "next": "3", "value": 2},
      {"id": "3", "next": "4", "value": 3},
      {"id": "4", "next": "5", "value": 4},
      {"id": "5", "next": "6", "value": 5},
      {"id": "6", "next": "7", "value": 6},
      {"id": "7", "next": "8", "value": 7},
      {"id": "8", "next": "9", "value": 8},
      {"id": "9", "next": null, "value": 9}
    ]
  }

const k = 4

removeKthNodeFromEnd(head, k) // {
//     "head": "0",
//     "nodes": [
//       {"id": "0", "next": "1", "value": 0},
//       {"id": "1", "next": "2", "value": 1},
//       {"id": "2", "next": "3", "value": 2},
//       {"id": "3", "next": "4", "value": 3},
//       {"id": "4", "next": "5", "value": 4},
//       {"id": "5", "next": "7", "value": 5},
//       {"id": "7", "next": "8", "value": 7},
//       {"id": "8", "next": "9", "value": 8},
//       {"id": "9", "next": null, "value": 9}
//     ]
//   }