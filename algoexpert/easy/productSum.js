function productSum(array, prod = 1) {
    // suma final
	let sum = 0
	// iterar por cada elemento
	for(const element of array) {
		// si es array aplicamos recursion con el producto
		if (Array.isArray(element)) {
			sum += productSum(element, prod + 1)
        // sino lo sumamos
		} else {
			sum += element
		}
	}
	// la recursion me devuelve esto
	return sum * prod
}

const array = [5, 2, [7, -1], 3, [6, [-13, 8], 4]]

console.log(productSum(array));