function findClosestValueInBst(tree, target) {
    return findClosestValueHelper(tree, target, tree.value);
}

function findClosestValueHelper(tree, target, closest) {
    let currentNode = tree;
    // va cambiando el nodo
    while (currentNode !== null) {
        // si 12 - primer valor inicializado es mayor a 12 - nodo actual
        if (Math.abs(target - closest) > Math.abs(target - currentNode.value)) {
            closest = currentNode.value;
        }
        // 12 menor al nodo actual
        if (target < currentNode.value) {
            // nos vamos a la izquierda
            currentNode = currentNode.left;
        // sino a la derecha
        } else if (target > currentNode.value) {
            currentNode = currentNode.right;
        } else {
            break;
        }
    }
    return closest;
}

class BST {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
}

const tree = {
    "nodes": [
      {"id": "10", "left": "5", "right": "15", "value": 10},
      {"id": "15", "left": "13", "right": "22", "value": 15},
      {"id": "22", "left": null, "right": null, "value": 22},
      {"id": "13", "left": null, "right": "14", "value": 13},
      {"id": "14", "left": null, "right": null, "value": 14},
      {"id": "5", "left": "2", "right": "5-2", "value": 5},
      {"id": "5-2", "left": null, "right": null, "value": 5},
      {"id": "2", "left": "1", "right": null, "value": 2},
      {"id": "1", "left": null, "right": null, "value": 1}
    ],
    "root": "10"
  }
const target = 12

// sol 13