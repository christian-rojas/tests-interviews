function findThreeLargestNumbers(array) {
	while (array.length > 3) {
        // encontrar el minimo
        const min = Math.min(...array)
        // encontrar su indice
        const find = array.indexOf(min)
        // eliminarlo del array
        array.splice(find, 1)
	}
}

const array = [141, 1, 17, -7, -17, -27, 18, 541, 8, 7, 7] 

console.log(findThreeLargestNumbers()); // 18, 141, 141