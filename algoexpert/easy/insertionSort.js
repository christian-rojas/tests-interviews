function insertionSort(array) {
	for (let i = 1; i < array.length; i++) {
		let j = i;
		// si el actual es menor al anterior
		while (j > 0 && array[j] < array[j - 1]) {
			swap(j, j - 1, array);
			j -= 1;
		}
	}
	return array;
}
// actual, anterior
function swap(i, j, array) {
    // temp: anterior que es mayor
	const temp = array[j];
    // anterior por el actual que es menor array[i]
	array[j] = array[i];
    // actual se va el mayor
	array[i] = temp;
}

console.log(insertionSort([8, 5, 2, 9, 5, 6, 3])); // [2, 3, 5, 5, 6, 8, 9]

// bubble sort es mas lento que este
