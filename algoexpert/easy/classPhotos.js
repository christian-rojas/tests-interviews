function classPhotos(redShirtHeights, blueShirtHeights) {
	// de mayor a menor
	blueShirtHeights.sort((a, b) => b - a);
	redShirtHeights.sort((a, b) => b - a);

	// de que color son los de primera fila?
	const shirtColorInFirstRow =
		redShirtHeights[0] < blueShirtHeights[0] ? 'RED' : 'BLUE';
	// itera por los de polera roja
	for (let idx = 0; idx < redShirtHeights.length; idx++) {
		const redShirtHeight = redShirtHeights[idx];
		const blueShirtHeight = blueShirtHeights[idx];
		// si son rojos
		if (shirtColorInFirstRow === 'RED') {
			// y el rojo es mas alto que el azul, false
			if (redShirtHeight >= blueShirtHeight) return false;
			// sino, los de primera fila son azules
		} else if (blueShirtHeight >= redShirtHeight) return false;
	}
	// si todos los altos quedan atras retorna true
	return true;
}

console.log(classPhotos([5, 8, 1, 3, 4], [6, 9, 2, 4, 5]));
