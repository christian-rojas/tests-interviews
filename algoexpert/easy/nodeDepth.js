function nodeDepths(root, depth = 0) {
	if (root === null) return 0;
	return (
		depth +
		nodeDepths(root.left, depth + 1) +
		nodeDepths(root.right, depth + 1)
	);
}

// o 
// const left = nodeDepths(root.left)
// const right = nodeDepths(root.right)
// return Math.max(left, right) + 1

// This is the class of the input binary tree.
class BinaryTree {
	constructor(value) {
		this.value = value;
		this.left = null;
		this.right = null;
	}
}
