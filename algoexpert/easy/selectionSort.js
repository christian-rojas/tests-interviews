function selectionSort(array) {
	let startIdx = 0;
	// itera de uno en uno
	while(startIdx < array.length - 1) {
		// setea el menor id, comienza en cero
		let smallestIdx = startIdx;
		// itera desde el siguiente numero
		for (let i = startIdx + 1; i < array.length; i++) {
			// se busca el menor numero
			if(array[smallestIdx] > array[i]){
				smallestIdx = i;
			}
		}
		// el menor pasa a la iteracion actual
		swap(startIdx, smallestIdx, array);
		startIdx++;
	}
	return array;
}

function swap(i, j, array) {
	const temp = array[j];
	array[j] = array[i];
	array[i] = temp;
}

console.log(selectionSort([8, 5, 2, 9, 5, 6, 3])); // [2, 3, 5, 5, 6, 8, 9]