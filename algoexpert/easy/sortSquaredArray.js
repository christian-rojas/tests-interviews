function sortedSquaredArray(array) {
    // return squared sorted array
    const last = array.map(e => Math.pow(e, 2))
    return last.sort((a,b) => a - b)
}

console.log(sortedSquaredArray([1, 2, 3, 5, 6, 8, 9]))