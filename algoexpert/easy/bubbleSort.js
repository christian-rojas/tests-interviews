function bubbleSort(array) {
	let isSorted = false
	let counter = 0
	// check si esta ordenado
	while (!isSorted) {
		// set true por si no entro al buvle
		isSorted = true
		// itero en base a mi posicion actual
		for(let i=0; i < array.length - 1 - counter; i++) {
			if(array[i] > array[i + 1]) {
				swap(i, i + 1, array)
				isSorted = false
			}
		}
		counter++;
	}
	return array
}
 function swap(i, j, array) {
	 const temp = array[j]
	 array[j] = array[i]
	 array[i] = temp
 }

 // es -counter por que quedan los mayores al final en cada vuelta del for

 console.log(bubbleSort([8, 5, 2, 9, 5, 6, 3])); // [2, 3, 5, 5, 6, 8, 9]
