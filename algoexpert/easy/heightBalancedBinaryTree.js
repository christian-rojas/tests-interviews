// a binary tree is height balanced if
// for each node in the tree the difference between
// the height of its left subtree and the height of
// its right subtree is at most 1
class TreeInfo {
  constructor(isBalanced, height) {
    this.isBalanced = isBalanced;
    this.height = height;
  }
}

function heightBalancedBinaryTree(tree) {
  const treeInfo = getTreeInfo(tree);
  return treeInfo.isBalanced;
}

function getTreeInfo(node) {
  // si es null entonces es balanceado
  if (node === null) return new TreeInfo(true, -1);
  // recursivamente recorrer el lado izquierdo
  const leftSubtreeInfo = getTreeInfo(node.left);
  // recursivamente recorrer el lado derecho
  const rightSubtreeInfo = getTreeInfo(node.right);
  // esta balanceado 
  const isBalanced =
    leftSubtreeInfo.isBalanced &&
    rightSubtreeInfo.isBalanced &&
    Math.abs(leftSubtreeInfo.height - rightSubtreeInfo.height) <= 1;
  // altura maxima del nodo
  const height = Math.max(leftSubtreeInfo.height, rightSubtreeInfo.height) + 1;
  // retorna el valor del arbol
  return new TreeInfo(isBalanced, height);
}

const tree = {
  tree: {
    nodes: [
      { id: '1', left: '2', right: '3', value: 1 },
      { id: '2', left: '4', right: '5', value: 2 },
      { id: '3', left: null, right: '6', value: 3 },
      { id: '4', left: null, right: null, value: 4 },
      { id: '5', left: '7', right: '8', value: 5 },
      { id: '6', left: null, right: null, value: 6 },
      { id: '7', left: null, right: null, value: 7 },
      { id: '8', left: null, right: null, value: 8 },
    ],
    root: '1',
  },
};

heightBalancedBinaryTree(tree);
