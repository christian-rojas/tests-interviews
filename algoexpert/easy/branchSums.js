class BinaryTree {
    constructor(value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }
}

function branchSums(root) {
    // inicializar el array de sumas
    const sums = [];
    // llamar a la funcion
    calculateBranchSums(root, 0, sums);
    return sums;
}

function calculateBranchSums(node, runningSum, sums) {
    if (!node) return;

    const newRunningSum = runningSum + node.value;
    // limite inferior
    if (!node.left && !node.right) {
        // agregar la suma
        sums.push(newRunningSum);
        // salir de la recurrencia
        return;
    }
    // ir por la izquierda
    calculateBranchSums(node.left, newRunningSum, sums);
    // ir por la derecha
    calculateBranchSums(node.right, newRunningSum, sums);
}
