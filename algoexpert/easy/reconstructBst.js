class BST {
    constructor(value, left = null, right = null) {
      this.value = value;
      this.left = left;
      this.right = right;
    }
  }
  class TreeInfo {
      constructor(rootIdx) {
          this.rootIdx = rootIdx
      }	
  }
  
  function reconstructBst(preOrderTraversalValues) {
    const treeInfo = new TreeInfo(0)
      const bst = reconstructBstFromRange(-Infinity, Infinity, preOrderTraversalValues, treeInfo)
      console.log(JSON.stringify(bst));
      return bst
  }
  
  function reconstructBstFromRange(lowerBound, upperBound, preOrderTraversalValues, currentSubtreeInfo) {
      if(currentSubtreeInfo.rootIdx === preOrderTraversalValues.length) return null
      const rootValue = preOrderTraversalValues[currentSubtreeInfo.rootIdx]
      if(rootValue < lowerBound || rootValue >= upperBound) return null
      
      currentSubtreeInfo.rootIdx++
      const leftSubtree = reconstructBstFromRange(lowerBound, rootValue, preOrderTraversalValues, currentSubtreeInfo)
      const rightSubtree = reconstructBstFromRange(rootValue, upperBound, preOrderTraversalValues, currentSubtreeInfo)
      return new BST(rootValue, leftSubtree, rightSubtree)
  }

  reconstructBst([10,4,2,1,5,17,19,18])
