function minimumWaitingTime(queries) {
    // Write your code here.
      queries.sort((a,b) => a-b);
      console.log(queries);
      let totalWaitingTime = 0;
      for(let idx = 0; idx < queries.length; idx++){
          const duration = queries[idx];
          const queriesLeft = queries.length - (idx + 1);
        //   console.log(queriesLeft);
          totalWaitingTime += duration * queriesLeft;
          console.log(totalWaitingTime);
      }
    return totalWaitingTime;
  }

console.log(minimumWaitingTime([3, 2, 1, 2, 6]))

  //example [1,4,5]: [0, 5, 5+1] = 11