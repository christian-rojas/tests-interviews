class LinkedList {
    constructor(value) {
      this.value = value;
      this.next = null;
    }
  }
  
  function removeDuplicatesFromLinkedList(linkedList) {
      // asigno el linkedlist a una variable
      let  currentNode = linkedList;
      while (currentNode !== null) {
          // obtengo la referencia al proximo
          let nextDistinctNode = currentNode.next;
          // mientras haya un siguiente y el valor sea igual
          while(nextDistinctNode != null && nextDistinctNode.value === currentNode.value) {
              // reemplazar el siguiente por el subsiguiente
              nextDistinctNode = nextDistinctNode.next;
          }
          // la referencia al proximo es el siguiente (que ya cambio)
          currentNode.next = nextDistinctNode;
          // el current es el siguiente
          currentNode = nextDistinctNode;
      }
    return linkedList;
  }

const test = {
    "linkedList": {
      "head": "1",
      "nodes": [
        {"id": "1", "next": "1-2", "value": 1},
        {"id": "1-2", "next": "1-3", "value": 1},
        {"id": "1-3", "next": "2", "value": 1},
        {"id": "2", "next": "3", "value": 3},
        {"id": "3", "next": "3-2", "value": 4},
        {"id": "3-2", "next": "3-3", "value": 4},
        {"id": "3-3", "next": "4", "value": 4},
        {"id": "4", "next": "5", "value": 5},
        {"id": "5", "next": "5-2", "value": 6},
        {"id": "5-2", "next": null, "value": 6}
      ]
    }
  }

console.log(removeDuplicatesFromLinkedList(test))
