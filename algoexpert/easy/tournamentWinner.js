function tournamentWinner(competitions, results) {
    // Write your code here.
    const collection = new Map();
    competitions.map((e, index) => {
        if (results[index] === 1) {
            collection.set(
                e[1],
                collection.get(e[1]) ? collection.get(e[1]) + 0 : 0
            );
            collection.set(
                e[0],
                collection.get(e[0]) ? collection.get(e[0]) + 3 : 3
            );
        } else {
            collection.set(
                e[0],
                collection.get(e[0]) ? collection.get(e[0]) + 0 : 0
            );
            collection.set(
                e[1],
                collection.get(e[1]) ? collection.get(e[1]) + 3 : 3
            );
        }
        return null;
    });
    const max = Math.max(...collection.values());
    const find = [...collection].find(([key, val]) => val === max);
    return find[0];
}
const competitions = [
    ['HTML', 'C#'],
    ['C#', 'Python'],
    ['Python', 'HTML'],
];

const results = [0, 0, 1];
// the 1 result means the home team won
console.log(tournamentWinner(competitions, results));
