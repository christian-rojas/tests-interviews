function getNthFib(n) {
    //  el segundo elemento siempre es 1
	if (n === 2) {
		return 1;
    // si es 1 es 0
	} else if (n === 1) {
		return 0;
    // retorna la suma
	} else {
        // 5: 2 + 3
		return getNthFib(n - 1) + getNthFib(n - 2);
	}
}

console.log(getNthFib(6))
