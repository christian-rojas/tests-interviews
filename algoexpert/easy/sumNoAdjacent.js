function maxSumNoAdjacent(array) {
    if(!array.length) return 0
    if(array.length === 1) return array[0]
    // define 
    let second = array[0] // first
    // definimos el elemento de partida, que es la suma total (current)
    let first = Math.max(array[0], array[1])
    //recorrer desde el tercer elemento 
    for(let i = 2; i< array.length; i++) {
        // el mayor entre el primer conjunto y 
        console.log(first, second, second + array[i]);
        const current = Math.max(first, second + array[i])
        // se asigna la suma anterior
        second = first
        // se asign la suma actual
        first = current
    }

    return first
}

console.log(maxSumNoAdjacent([7, 10, 12, 7, 9, 14]))