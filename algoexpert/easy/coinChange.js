function nonConstructibleChange(coins) {
    // ordenar de menor a mayor
    coins.sort((a, b) => a - b);

    let currentChange = 0;
    for (const coin of coins) {
        if (coin > currentChange + 1) return currentChange + 1;
        // menor o igual, entonces si puedo crear esos cambios
        currentChange += coin;
    }
    // si no tengo monedas retorno 1
    return currentChange + 1;
}
