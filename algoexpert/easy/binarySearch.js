function binarySearch(array, target) {
	return binarySearchHelper(array, target, 0, array.length - 1);
}

function binarySearchHelper(array, target, left, rigth) {
	// se acabo el array
	if (left > rigth) return -1;
	// buscar el de en medio
	const middle = Math.floor((left + rigth) / 2);
	// valor de el de en medio
	const potentialMatch = array[middle];
	// si lo encuentra lo retorna
	if (target === potentialMatch) {
		return middle;
		// sino, si el target es menor
	} else if (target < potentialMatch) {
		// busco a la izquierda
		return binarySearchHelper(array, target, left, middle - 1);
	} else {
		// busco a la derecha
		return binarySearchHelper(array, target, middle + 1, rigth);
	}
}

console.log(binarySearch([0, 1, 21, 33, 45, 45, 61, 71, 72, 73], 33)); //3