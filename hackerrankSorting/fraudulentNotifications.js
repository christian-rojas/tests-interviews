'use strict'

// function activityNotifications(expenditure, d) {
//     let idx = 0, notification = 0
//     for(let i=d; i < expenditure.length; i++) {
//         const currArr = expenditure.slice(idx, d+idx)
//         const median = (arr: any) => {
//             let middle = Math.floor(arr.length / 2);
//               arr = [...arr].sort((a, b) => a - b);
//             return arr.length % 2 !== 0 ? arr[middle] : (arr[middle - 1] + arr[middle]) / 2;
//         };
//         const mediana = median(currArr)
//         if(expenditure[i] >= mediana*2) notification++
//         idx++
//     }
//     return notification
    
// }

console.log(activityNotifications([10, 20, 30, 40, 50], 3));


function getMedianx2(countArr, days) {
    let sum = 0
    for (let i = 0; i < countArr.length; i++) {
      sum += countArr[i]
      if (sum * 2 === days) return (i * 2 + 1)
      if (sum * 2 > days) return (i * 2)
    }
  }
  function activityNotifications(debits, days) {
    const countArr = new Array(201).fill(0)
    let notices = 0
    for (let i = 0; i < days; i++) {
      countArr[debits[i]]++
    }
    for (let i = days; i < debits.length; i++) {
      const medianx2 = getMedianx2(countArr, days)
      if (debits[i] >= medianx2) notices++
      if (i === debits.length - 1) break
      countArr[debits[i - days]]--
      countArr[debits[i]]++
    }
    return notices
  }

// console.log(activityNotifications([1, 2, 3, 4, 4], 4));

/*
On the first three days, they just collect spending data. At day , trailing expenditures are .
 The median is  and the day's expenditure is . Because , there will be a notice.
  The next day, trailing expenditures are  and the expenditures are .
   This is less than  so no notice will be sent.
    Over the period, there was one notice sent.
*/