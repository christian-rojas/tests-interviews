const calPoints = function(ops) {
    let arr = []
    for(const item of ops) {
        if(/^0$|^-?[1-9]\d*(\.\d+)?$/.test(item)) {
            arr.push(item)
        }else if(item === "C") {
            arr.pop()
        }else if(item === "D") {
            arr.push((arr[arr.length - 1] * 2).toString())
        }else if(item === "+") {
            arr.push(arr.reduce((sum, el) => Number(sum) + Number(el)).toString())
        }
    }
    return arr.reduce((sum, el) => Number(sum) + Number(el)).toString();
}

calPoints(["5", "-2", "4", "C", "D", "9", "+", "+"])