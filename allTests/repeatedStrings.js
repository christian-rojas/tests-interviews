function repeatedString(s, n) {
    // veces que aparece la "a" = [ '', 'b', '' ]
    let occurances = (s.split("a").length - 1);
    // occurances = 2
    let max = Math.floor(n / s.length);
    // 10 / 3 = 3
    let totalAs= occurances * max;
    // 6
    // cortar de 0 a 10 % 3 = 3
    // encontrar la cantidad de "a"
    // cuando no se puede dividir
    totalAs += (s.slice(0, n % s.length).split("a").length - 1);
    // 7
    return totalAs;
}

repeatedString('aba', 10)