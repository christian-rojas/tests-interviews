function preprocessDate(dates) {
    const months = [
        "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    let responseDates = []
    
    for(let i=0; i< dates.length; i++) {
        let [day, month, year] = dates[i].split(' ');
        day = day.replace(/\D/g, '').padStart(2, '0');
        month = (months.indexOf(month) + 1).toString().padStart(2, '0');
        responseDates.push(`${year}-${month}-${day}`);
    }
    
    return responseDates
    
}

console.log(preprocessDate(["1st Mar 1984", "2nd Feb 2013", "14th Apr 1900"]))