const calPoints = (ops) => {
    let stack = [];
    for(let i=0; i<ops.length; i++){
    if(ops[i] == 'D'){
        stack.push(2 * stack.slice(-1)[0]);
    }else if(ops[i] === 'C'){
         stack.pop();
    }else if(ops[i] === '+'){
        stack.push(stack.slice(-1)[0] + stack.slice(-2)[0])
     }else{
         stack.push(parseInt(ops[i]));
     }
        
    }
    return stack.reduce((a,b)=>a+b);
}

console.log(calPoints(["5", "2", "C", "D", "+"]))

