function maximumQuality(packets, channels) {
    /// Write your code here
    let n = packets.length
    
    let answer = 0
    if(n === channels) {
        for(let i = 0; i<n; i++) {
            answer += packets[i]
        }
        return answer
    }
    packets.sort((a,b) => a - b )
    
    for(let i = n - channels+1; i<n; i++) {
        answer += packets[i]
    }
    
    n = n-channels;
    const midPoint = Math.floor(n / 2);
    if(n % 2 === 0) {
        // odd
        answer += packets[n/2];
        console.log(answer);
    }
    else {
        //even
        let value = packets[midPoint] + packets[midPoint + 1];
        answer += value/2;
    }
    return Math.ceil(answer);
    

}

// maximumQuality([89, 48, 14], 3)

maximumQuality([2, 2, 1, 5, 3], 2)