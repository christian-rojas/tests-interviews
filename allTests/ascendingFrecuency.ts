/*
Given an array of integers nums, sort the array in increasing order based on the frequency of the values.
If multiple values have the same frequency, sort them in decreasing order.
Return the sorted array.
*/

function itemsSort(items: number[]): number[] {
    const freq: Record<number, number> = {};
    
    items.forEach(num => freq[num] = (freq[num] || 0) + 1);

    return items.sort((a, b) => freq[a] != freq[b] ? freq[a] - freq[b] : a - b);
}