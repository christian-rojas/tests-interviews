function findPasswordStrength(password) {
    let count = 0
    let hashMap = new Map()
    for (let i = 0; i < password.length; i++) {
        // agrego el caracter como key y la posicion como value
        hashMap.set(password[i], i + 1)
        for (let el of hashMap.values()) {
            count += el
        }
    }
    return count
}

findPasswordStrength("test") //t, e, s, t, te, es, st, tes, est, test, et, ts, se, tt, tse, tet, set, tte, tts, sst

// findPasswordStrength("abc") // a, b, c, ab, ac, bc, abc, ca, ba, cba

// findPasswordStrength("good") // g, o, o, d, go, oo, od, goo, ood, good 16