function countingValleys(steps, path) {
    let sea = 0;
    let result = 0;
    let bajando = 0;
    for(let i = 0; i < steps; i++) {
        if(path[i] === 'U') {
            sea++
        }else {
            sea--
        }
        if(sea < 0) {
            bajando++
        }
        if(sea === 0 && i !== 0 && bajando > 0) {
            bajando = 0
            sea = 0
            result++
        }
    }
    return result

}