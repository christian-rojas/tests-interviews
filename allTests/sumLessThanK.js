function sum(array, K) {
    const map = new Map()
    for(let i=0; i < array.length; i++) {
        for(let j=i+1; j < array.length; j++) {
            // console.log(array[i], array[j])
            const current = array[i] + array[j]
            const closest = K - current
            console.log(current, closest);
            map.set(current, closest)
        }
    }

    let min = [...map.values()].filter(k => k > 0)
    min = Math.min(min)

}

sum([1,2,3,4,5], 4)