let findPermutations = (string: any): any => {
    if (!string || typeof string !== "string"){
      return "Please enter a string"
    } else if (string.length < 2 ){
      return string
    }
  
    let permutationsArray = [] 
     
    for (let i = 0; i < string.length; i++){
      let char = string[i]
      // si el caracter es distinto al de la posicion actual, nos saltamos la iteracion actual
      if (string.indexOf(char) != i)
      continue
      // el parcial desde el inicio hasta la posicion actual mas la posicion actual mas 1 hasta el final
      let remainingChars = string.slice(0, i) + string.slice(i + 1, string.length) // a + bc
      console.log(remainingChars);
      // recursion: por cada elemento 
      for (let permutation of findPermutations(remainingChars)){
        permutationsArray.push(char + permutation) }
    }
    return permutationsArray
  }

console.log(findPermutations("aabc"));

// ts-node permutations.ts