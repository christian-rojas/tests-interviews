// minimumSwaps function to sort asc array.
function minimumSwaps(arr) {
    let swapCount = 0;
    for (let index = 0; index < arr.length; index++) {
        // elemento actual
        const currentInt = arr[index];
        // posicion absoluta
        const position = index + 1;
        // si el elemento no es igual a su posicion abosluta, swap
        if (currentInt !== position) {
            let indexToSwap
            for (let i = 0; i < arr.length; i++) {
                // busca la posicion donde deberia estar
                if (arr[i] === position) {
                    indexToSwap = i;
                    break
                }
            }
            // asigna el elemento actual a su posicion
            arr[indexToSwap] = currentInt;
            // reemplaza el valor a su posicion
            arr[index] = position;
            swapCount = swapCount + 1;
        }
    }
    return swapCount;
}